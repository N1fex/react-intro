class Car extends React.Component {

  componentWillUnmount() {
    console.log('end');
  }

  render(){
    return (<div className={this.getClasses()} onClick={this.props.onMark}>
      <div className="card-img" onClick={() => this.setState({up: !this.state.up})}>
        <img
          src={this.props.car.img}
          alt={this.props.car.name}/>
      </div>
      <h3>{this.props.car.name}</h3>
      <p>{this.props.car.price} $</p>
    </div>)
  }

  getClasses() {
    let classes = ['card'];
    if (this.props.car.marked) classes.push('marked')
    return classes.join(' ')
  }
}

class App extends React.Component {
  state = { // state - зарезервированное слово
    cars: [],
    visible: true,
    appTitle: 'Cars',
    search: ''
  };

  componentDidMount() {
    this.setState({cars: this.initCars()})
  }

  render() {
    const style = {
      marginLeft: 20,
      marginRight: 20
    }
    return (
      <div className="app">
        <h1>{this.state.appTitle}</h1>
        <button onClick={() => this.toggleHandler()}>Toggle</button>
        <input type="text"
               style={style}
               placeholder={'Change title'}
               onChange={(event) => this.changeHandler(event.target.value)}
               value={this.state.appTitle}
        />
        <input type="text"
               placeholder={'Search'}
               onChange={e => this.setState({search: e.target.value})}
        /> &nbsp;&nbsp;&nbsp;
        <button onClick={() => this.findCars(this.state.search)}>Find</button>


        <hr/>
        <div className="list">
          {this.renderCars()}
        </div>
      </div>
    )
  }

  initCars() {
    return [
      {
        marked: false,
        name: 'BMW M2 Coupe',
        price: 20000,
        img: 'https://mochamanstyle.com/wp-content/uploads/2015/10/2016-BMW-M2-Coupe.jpg'
      },
      {
        marked: false,
        name: 'Audi TT',
        price: 15000,
        img: 'https://article.images.consumerreports.org/w_598,h_436/prod/content/dam/cro/news_articles/cars/2016-Audi-TT-pr-1-2016-598'
      },
      {marked: false, name: 'Rolls Royce', price: 50000, img: 'https://a.d-cd.net/13cec6s-1920.jpg'},
      {
        marked: false,
        name: 'Mercedes amg coupe',
        price: 18000,
        img: 'https://auto.ndtvimg.com/car-images/big/mercedes-amg/gle-coupe/mercedes-amg-gle-coupe.jpg?v=2'
      }
    ]
  }

  handleMarked(name) {
    const cars = this.state.cars.concat();
    const car = cars.find(c => c.name === name)
    car.marked = !car.marked
    this.setState({cars})
  }

  toggleHandler() {
    this.setState({visible: !this.state.visible})
  }

  renderCars() {
    if (!this.state.visible) return null;

    return this.state.cars.map(car => (
      <Car
        car={car}
        key={car.name + Math.random()}
        onMark={this.handleMarked.bind(this, car.name)}
      />
    ))
  }

  changeHandler(title) {
    if (title.trim() === '') {
      return
    }
    this.setState({appTitle: title})
  }

  /*filterCars() {
    let cars = this.state.cars;
    return this.state.search === '' ?
      cars :
      cars.filter(car => car.name.toLowerCase().includes(this.state.search.toLowerCase()))
  }*/

  findCars(title) {
    let filteredCars = this.initCars().filter(car => {
      if (car.name.toLowerCase().includes(title.toLowerCase())) return car
    })

    if (title === '') this.setState({cars: this.initCars()})
    else this.setState({cars: filteredCars})
  }
}

ReactDOM.render(<App/>, document.getElementById('root')) // метод render складывает всё необходимое в контейнер
